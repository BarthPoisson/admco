#Exo1 et 2 

Création du code de notre calculatrice sous forme de classe.

#Exo3

On possède une note de 4.11/10 : il faut rajouter les docstrings
La correction complete pour obtenir une note de 10/10 se trouce dans le fichier "python_code_exo2_10sur10.py"

#Exo4

export PYTHONPATH=$PYTHONPATH:"."    //Ajoute calculator dans le path 

#Exo5

On test le type des valeurs via :  "if isinstance(var_a, int) and isinstance(var_b, int):"
si la condition n'est pas validée on a une erreur.

#Exo6

On a mis en place un ensemble de tests via "unittest" dans notre code de tests 4 classes de tests:
    >class AdditionTest (unittest.TestCase):
  >class DifferenceTest (unittest.TestCase):
  >class MultiplicationTest (unittest.TestCase):
  >class DivisionTest (unittest.TestCase):

que l'on appelle via  "unittest.main()"


#Exo7

On a ajouté des messages personnalisés pour l'utilisateur via "import logging"

#Exo8

- Lancé la phase de test à partir du setup avec la commande: python setup.py test
- Archiver à partir du setup: python setup.py sdist
- Séparer la partie fonctionelle de la phase de test avec la commande: python setup.py bdist_wheel

Generating distribution archives:

    - Generate distribution packages for the package
    python3 -m pip install --upgrade build

    - Generate two files in the dist directory:

    dist/
      example_pkg_YOUR_USERNAME_HERE-0.0.1-py3-none-any.whl
      example_pkg_YOUR_USERNAME_HERE-0.0.1.tar.gz
    python3 -m build

    - Use twine to upload the distribution packages.
    python3 -m pip install --user --upgrade twine

    - Once installed, run Twine to upload all of the archives under dist:
    python3 -m twine upload --repository testpypi dist/*

    For the username, use __token__. For the password, use the token value, including the pypi- prefix.

    After the command completes, you should see output similar to this:
    Uploading distributions to https://test.pypi.org/legacy/
    Enter your username: [your username]
    Enter your password:
    Uploading example_pkg_YOUR_USERNAME_HERE-0.0.1-py3-none-any.whl
    100%|█████████████████████| 4.65k/4.65k [00:01<00:00, 2.88kB/s]
    Uploading example_pkg_YOUR_USERNAME_HERE-0.0.1.tar.gz
    100%|█████████████████████| 4.25k/4.25k [00:01<00:00, 3.05kB/s]

"Sources: https://packaging.python.org/tutorials/packaging-projects/#packaging-your-project"

#Exo10

Utilisation de Pypi afin d'upload notre package. 
On obtient notre lien afin d'installer notre package precedement créé: "pip install -i https://test.pypi.org/simple/ calculatorV2"


Administration système et gestion de codes – DS
Crée un projet sur gitlab

Sign in
New project
Create blank project
Mettre le nom du projet
Se mettre en public + cocher la case read me + Create project
Ajout de membre : voir à gauche, case members, rechercher le membre, changer le rôle permission à maintener + invite
Lié projet gitlab et ordi 
Créer un dossier dans le bureau avec le nom (peu importe dans notre cas MyTestProject)
Ouvrir un terminal et se placer dans le dossier que nous venons de créer : cd Bureau/MyTestProject
	Crée le dépôt git : git init
Création d’une clé ssh si besoin
	Dans le terminal : ssh-keygen -t rsa -b 2048 -C "<comment>"

  

	Note : on n’écrit rien, on fait juste des entrées.
	Copier la clé dans le presse papier pour le mettre ensuite dans gitLab : sudo apt 	install xclip et xclip -sel clip < ~/.ssh/id_ed25519.pub  
	 Mettre la clé sur gitLab, aller sur gitLab, aller en haut à droite sur notre photo de 	profil 🡪 préférence 🡪 ssh key 🡪 puis coller la clé 🡪 addkey 

Lié gitlab et dossier vm:  (toujours dans le terminal) git clone + clé ssh

Aller dans le projet gitLab : cd Nomduprojetgitlab
	Zone de test de la liaison : 
	On modifie le fichier readme sur gitlab sans oublier de commit puis dans le terminal : 	git pull
	modifier le read me : nano READ.me  on modifie et CTRL+S et CTRL+X
	git add . git commit . -m « test » 
	git push
	note : si ca ne marche pas de panique on recommence à partir de git init
	
Package python
Aller sur gitLab : 
Cliquer sur le + et add new file : requirement.txt ( pour le pipeline après, attention au nom des fichier si ça ne fonctionne pas)
	                          et Setup.py
				
Cliquer sur le + et New directory : test
				    	       Package_test

Ensuite dans test et package_test on crée un nouveau fichier : __init__.py (un dans chacun des dossiers)
Dans test on crée les fichiers python : Librairy_test.py (dans test, le mot librairie on peut mettre ce que on veut ) 



Et dans package_test on crée Morpion.py



Ecrire dans le terminal : pip install setuptools pour installer setuptools
Dans le setup, écrire : 

    



Puis dans Morpion.py et Library_test. Py mettre au début du code : 
#!usr/bin/env python 
#coding : utf-8

Tips : Dans Morpion.py, on code nos fonctions et éventuellement si le sujet dit plusieurs modules on crée un autre module(fichier .py ) dans le package_test que on peut appeler Morpion2.py par exemple
Au début du fichier librairy_test.py mettre :
import unittest // essentiel pour faire les tests
from package_test import Morpion // on importe notre classe
Si jamais en executant les test on obtient module names “Morpion” ou “package_test” inconnu : ne pas avoir oublié pip install ., ou vérifier le nom des packages dans le setup.py
Se placer dans librairy_test.py et coder les tests (cf page test) 
Voir si les tests sont valides dans le terminal de la vm : -se placer dans le dossier où est le setup
pip install . 
python(ou python3) ./test/Librairy_test.py
Regarder si problème dans les tests si non RUN OK et si oui changer le code de test ( FAILED)


IV. Faire le pipeline ( gitlab)
Aller sur gitLab -> colonne de gauche -> CI/CD -> pipelines->Get started
créer un nouveau fichier : .gitlab-ci.yml à l’origine

Dans requirement.txt ecrire: 
pylint==1.8.3
twine>=1.15.0
setuptools>=44.1.1
pytest==4.6.11
Ensuite retourner dans pipelines sur gitlab et checker que c’est valider :) 
Installations : 
pip install pylint (note du code)
pip install xclip (pour clef SSH)
pip install setuptools (pour le setup)

