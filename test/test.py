#!/usr/bin/ev python
# coding: utf-8

from calculator.exo2 import Calcul
import unittest
import logging

##TEST##
class AdditionTest(unittest.TestCase):
    def setUp(self):
        """Executed before every test case"""
        self.calculator = Calcul()

    def test_add_two_integers(self):
        result = self.calculator.add(5, 5)
        self.assertEqual(result, 10)
        logging.info("Nickel")

    def test_add_integers_string(self):
        result = self.calculator.add(5, "6")
        self.assertEqual(result, "ERROR")
        logging.error("Oups, ça n’a pas marché comme prévu.")

    def test_add_negative_integers(self):
        result = self.calculator.add(-5, -5)
        self.assertEqual(result, -10)
        logging.info("Nickel")
        self.assertNotEqual(result, 10)


class DifferenceTest(unittest.TestCase):
    def setUp(self):
        """Executed before every test case"""
        self.calculator = Calcul()

    def test_substract_two_integers(self):
        result = self.calculator.soustract(30, 5)
        logging.info("Nickel")
        self.assertEqual(result, 25)

    def test_substract_integers_string(self):
        result = self.calculator.soustract(5, "6")
        self.assertEqual(result, "ERROR")
        logging.error("Oups, ça n’a pas marché comme prévu.")

    def test_substract_negative_integers(self):
        result = self.calculator.soustract(-30, -5)
        logging.info("Nickel")
        self.assertEqual(result, -25)
        self.assertNotEqual(result, -35)


class MultiplicationTest(unittest.TestCase):
    def setUp(self):
        """Executed before every test case"""
        self.calculator = Calcul()

    def test_multiply_two_integers(self):
        result = self.calculator.multiply(5, 5)
        logging.info("Nickel")
        self.assertEqual(result, 25)

    def test_multiply_integers_string(self):
        result = self.calculator.multiply(5, "6")
        self.assertEqual(result, "ERROR")
        logging.error("Oups, ça n’a pas marché comme prévu.")

    def test_multiply_negative_integers(self):
        result = self.calculator.multiply(-5, -5)
        logging.info("Nickel")
        self.assertEqual(result, 25)
        self.assertNotEqual(result, -25)


class DivisionTest(unittest.TestCase):
    def setUp(self):
        """Executed before every test case"""
        self.calculator = Calcul()

    def test_division_two_integers(self):
        result = self.calculator.divide(30, 5)
        logging.info("Nickel")
        self.assertEqual(result, 6)

    def test_division_integers_string(self):
        result = self.calculator.divide(5, "6")
        logging.error("Oups, ça n’a pas marché comme prévu.")
        self.assertEqual(result, "ERROR")

    def test_division_negative_integers(self):
        result = self.calculator.divide(-30, -5)
        logging.info("Nickel")
        self.assertEqual(result, 6)
        self.assertNotEqual(result, -6)

    def divide_by_zero(self):
        result = self.calculator.divide(10, 0)
        self.assertEqual(result, "ERROR")
        with self.assertRaises(ZeroDivisionError):
            self.calculator.divide(10, 0)


if __name__ == "__main__":
    unittest.main()
