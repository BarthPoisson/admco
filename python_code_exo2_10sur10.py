""" code d'un simulateur de calcule basique """

#!/usr/bin/env python
# coding: utf-8

# les lignes de dessus sont la pour 1 dire que le fichier est un fichier python au systeme
#
from __future__ import print_function


class SimpleCalculator(object):
    """classe pour les calcules basique : fonction une , somme fonction deux,
    soustraction fonction trois, multiplication fonction quatre, division"""

    def __init__(self):
        return

    @staticmethod
    def sum(variable_1, variable_2):
        """ fait une somme """
        return variable_1 + variable_2

    @staticmethod
    def substract(variable_1, variable_2):
        """ fait une soustraction """
        return variable_1 - variable_2

    @staticmethod
    def multiply(variable_1, variable_2):
        """ fait une multiplication """
        return variable_1 * variable_2

    @staticmethod
    def divide(variable_1, variable_2):
        """ fait une division """
        return variable_1 / variable_2


### Test function ###

CONSTANT_UN = 5
CONSTANT_DEUX = 10
RESULTAT = SimpleCalculator()
print(RESULTAT.sum(CONSTANT_UN, CONSTANT_DEUX))
