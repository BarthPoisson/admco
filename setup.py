from setuptools import setup



setup(
    name = 'SimpleCalcul',
    version = '0.0.1',
    author = "THIRY Kevin",
    packages = ['SimpleCalcul'],
    description = 'Calculator est un package simple pour faire des test simple sur le package de python',
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    license = 'GNU GPLv3',
    python_requires = '>=2',
)
